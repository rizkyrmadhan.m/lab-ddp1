#muhamad rizky ramadhan
#2106652032
#lab 5

def penjumlahan() :
    ukuranMatriks = input("Masukan ukuran matriks : ")
    n = int(ukuranMatriks[0])
    m = int(ukuranMatriks[4])
    matriks1 = [[]]
    matriks2 = [[]]

    
    for i in range (n):
        baris = input("Baris {} matriks 1 : ".format(i + 1))
        angka = baris.split(" ")
        
        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks1.append(matriks)

    del matriks1[0]
    print("")

    
    for i in range (n):
        baris = input("Baris {} matriks 2 : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []
        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks2.append(matriks)

    del matriks2[0]
    print("")

    
    print("hasil dari operasi : ")
    for n in range(0, len(matriks1)):
        for m in range(0, len(matriks1[0])):
            print (matriks1[n][m] + matriks2[n][m], end=' '),
        print()


def pengurangan():
    ukuranMatriks = input("Masukan ukuran matriks : ")
    n = int(ukuranMatriks[0])
    m = int(ukuranMatriks[4])
    matriks1 = [[]]
    matriks2 = [[]]

    
    for i in range (n):
        baris = input("Baris {} matriks 1 : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks1.append(matriks)

    del matriks1[0]
    print("")

    
    for i in range (n):
        baris = input("Baris {} matriks 2 : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks2.append(matriks)

    del matriks2[0]
    print("")

    
    print("hasil dari operasi : ")
    for n in range(0, len(matriks1)):
        for m in range(0, len(matriks1[0])):
            print (matriks1[n][m] - matriks2[n][m], end=' '),
        print()


def transpose():
    ukuranMatriks = input("Masukan ukuran matriks : ")
    n = int(ukuranMatriks[0])
    m = int(ukuranMatriks[4])
    matriks1 = [[]]

    
    for i in range (n):
        baris = input("Baris {} matriks : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks1.append(matriks)

    del matriks1[0]
    print("")

    
    print("hasil dari operasi : ")
    for i in range(m):
        for j in range(n):
            print(matriks1[j][i], end = ' ')
        print()


def determinan() :
    n = 2
    m = 2
    matriks1 = [[]]

     
    for i in range (n):
        baris = input("Baris {} matriks : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks1.append(matriks)

    del matriks1[0]
    print("")

    
    print("hasil dari operasi : ")
    print((matriks1[0][0] * matriks1[1][1]) - (matriks1[0][1] * matriks1[1][0]))


def perkalian():
    n = 2
    m = 2
    matriks1 = [[]]
    matriks2 = [[]]

    
    for i in range (n):
        baris = input("Baris {} matriks 1 : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks1.append(matriks)

    del matriks1[0]
    print("")

    
    for i in range (n):
        baris = input("Baris {} matriks 2 : ".format(i + 1))
        angka = baris.split(" ")

        
        if len(angka) > m:
            print("Terjadi kesalahan input. Silahkan ulang kembali.\n")
            return

        matriks = []

        for j in range (m) :
            matriks.append(int(angka[j]))

        matriks2.append(matriks)

    del matriks2[0]
    print("")

    
    print("hasil dari operasi : ")
    print(matriks1[0][0] * matriks2[0][0] + matriks1[0][1] * matriks2[1][0], end = ' ')
    print(matriks1[0][0] * matriks2[0][1] + matriks1[0][1] * matriks2[1][1])
    print(matriks1[1][0] * matriks2[0][0] + matriks1[1][1] * matriks2[1][0], end = ' ')
    print(matriks1[1][0] * matriks2[0][1] + matriks1[1][1] * matriks2[1][1])


#main program
while (True) :
    print("Selamat datang di Matrin Calculator. Berikut adalah operasi operasi mang dapat dilakukan :")
    print("1. Penjumlahan")
    print("2. Pengurangan")
    print("3. Transpose")
    print("4. Determinan")
    print("5. Perkalian")
    print("6. Keluar")
    menu = int(input("Silahkan pilih operasi : "))

    if menu == 1 :
        penjumlahan()
    elif menu == 2 :
        pengurangan()
    elif menu == 3 :
        transpose()
    elif menu == 4 :
        determinan()
    elif menu == 5 :
        perkalian()
    elif menu == 6 :
        print("Sampai Jumpa!")
        break
    else :
        print("Masukan pilihan sesuai menu") 