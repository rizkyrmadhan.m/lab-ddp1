import random
import re

msg = input("Masukkan pesan: ")
n = input("Pilih nilai n: ")

msg = msg.lower()
n = int(n)

encrypted = ""
i = 0

file = open("paragraf.txt")
flag = 0
paragraf = file.read()
paragraf_enc = ""

alphabet = "abcdefghijklmnopqrstuvwxyz"
number = "0123456789"
# proses caesar chiper
while i < len(msg):
    if re.search(r"^\d$", msg[i]):
        new_char = (number.find(msg[i]) + n*2) % 10
        encrypted += number[new_char]
    elif msg[i] == ' ':
        encrypted += msg[i]
    else:
        new_char = (alphabet.find(msg[i]) + n) % 26
        encrypted += alphabet[new_char]
    i += 1


i = 0
while i < len(paragraf):
    if flag < len(encrypted):
        if encrypted[flag].isnumeric():
            paragraf_enc += encrypted[flag]
            flag += 1
            i -= 1
        elif encrypted[flag] == " ":
            paragraf_enc += '$'
            flag += 1
            i -= 1
        else:
            if encrypted[flag] == paragraf[i]:
                paragraf_enc += paragraf[i].upper()
                flag += 1
            else:
                paragraf_enc += paragraf[i]
    else:
        paragraf_enc += paragraf[i]
    i += 1

key_pos = random.randint(
    int(len(paragraf_enc)/2) - 250, int(len(paragraf_enc)/2)+250)

key = 20220310 - n
key = '' + str(key) + ''
secret_msg = paragraf_enc[:key_pos] + key + paragraf_enc[key_pos:]

f = open("secret_msg.txt", "w")
f.write(secret_msg)
f.close()