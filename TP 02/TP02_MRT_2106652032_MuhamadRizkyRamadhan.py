#Muhamad Rizky Ramadhan
#TP 02
#2106652032

import random
import re


def generateKalimat():
    file = open('hunger_games.txt', 'r')
    baris = file.read().split('.')

    index = random.randint(0, len(baris)-1)

    baris[index] = re.sub(r"[^0-9a-zA-Z\'\`\- ]",
                          "", baris[index]).strip().lower()

    file.close()
    return baris[index]


print("Selamat datang di Hangman Reborn!")
nyawa = int(input("Masukkkan jumlah nyawa: "))

print("\nSelamat bermain!")

jawaban = generateKalimat().split()

soal = random.sample(jawaban, len(jawaban))

kebenaran = 0

kalimat = []
for i in range(len(soal)):
    kalimat.append('_')

while nyawa > 0 and kebenaran < len(jawaban):
    print("\n===")

    print("Kalimat: ", end=' ')
    for i in range(len(kalimat)):
        print(kalimat[i], end=' ')

    print()
    print("Sisa nyawa anda: " + str(nyawa))

    print("Kata yang tersedia: ", end=' ')
    for i in range(len(soal)):
        print(soal[i], end=' ')

    tebakan = input("\nTebakan: ").split(' ')
    tebakKata = tebakan[0]
    tebakPos = tebakan[1]

    tebakPos = int(tebakPos)-1

    if tebakPos > -1 and tebakPos < len(jawaban):

        if jawaban[tebakPos] == tebakKata:
            print("Tebakan anda benar!")
            soal.remove(tebakKata)
            kalimat[tebakPos] = tebakKata
            kebenaran += 1
        else:
            print("Tebakan anda salah!")
            nyawa -= 1
    else:
        print("Posisi tidak valid")


print("\n===")
if nyawa == 0:
    print("Anda gagal menebak kalimatnya :(")
elif kebenaran == len(jawaban):
    print("Selamat! Anda berhasil menebak kalimatnya.")

print("Kalimat: ", end=' ')
for i in range(len(jawaban)):
    print(jawaban[i], end=' ')